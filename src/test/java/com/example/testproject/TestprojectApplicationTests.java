package com.example.testproject;

import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static junit.framework.TestCase.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
class TestprojectApplicationTests {

    @Autowired
    private WebDriver webDriver;

    @Before
    public void setup() {
        System.setProperty("webdriver.chrome.driver", "C:\\icons\\chromedriver\\chromedriver.exe");
        webDriver = new ChromeDriver();
    }

    @Test
    public void getSearchPage() {
        this.webDriver.get("https://www.google.com");
        WebElement element = this.webDriver.findElement(By.name("q"));
        assertNotNull(element);
    }

}
